﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(letsshare.Startup))]
namespace letsshare
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
